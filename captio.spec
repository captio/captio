Name:		captio
Version:	0.1
Release:	1%{?dist}
Summary:	Display and record stream of images from live camera
Group:          Application/Science

License:	GPLv2+
URL:		http://www.msc.univ-paris-diderot.fr/~daerr/
Source0:	captio-%{version}.tar.bz2

BuildRequires:	pkgconfig(glib-2.0) >= 2.26
BuildRequires:	pkgconfig(gobject-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(libxml-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(libnotify)
BuildRequires:	pkgconfig(gstreamer-base-0.10) >= 0.10.31
BuildRequires:	pkgconfig(gstreamer-app-0.10)
BuildRequires:	pkgconfig(gstreamer-interfaces-0.10)
BuildRequires:	desktop-file-utils

Source10:	captio.png

%global fullname %{name}-%{version}

%prep
%setup -q

%build
%configure --enable-notify
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
%find_lang %{fullname}
desktop-file-install --vendor=""                                 \
       --dir=%{buildroot}%{_datadir}/applications/   \
       %{buildroot}%{_datadir}/applications/captio.desktop

%files -f %{fullname}.lang
%{_bindir}/captio
%{_datadir}/%{fullname}/*.ui
%{_datadir}/icons/hicolor/22x22/apps/*
%{_datadir}/icons/hicolor/32x32/apps/*
%{_datadir}/icons/hicolor/48x48/apps/*
%{_datadir}/icons/hicolor/256x256/apps/*
%{_datadir}/applications/captio.desktop
/usr/doc/%{fullname}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor;
fi
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache -q %{_datadir}/icons/hicolor;
fi
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%changelog
