#ifndef CAPTIO_GUI_H
#define CAPTIO_GUI_H

typedef struct {// CaptioGUI
  guint rotation;
  gboolean flip_vertical;
  gboolean flip_horizontal;

  guint state; // GdkModifier mask of last input event in drawing area
  guint keysym; // GDK key code of last key-*-event in drawing area
  gdouble ox, oy; // Pointer coordinates at last button-press-event

  GtkWidget *main_window;
  GtkWidget *snapshot_button;
  GtkWidget *rotate_cw_button;
  GtkWidget *flip_vertical_toggle;
  GtkWidget *flip_horizontal_toggle;
  GtkWidget *drawing_area;
  GtkWidget *camera_combo_box;
  GtkWidget *trigger_combo_box;
  GtkWidget *frame_rate_entry;
  GtkWidget *exposure_spin_button;
  GtkWidget *gain_spin_button;
  GtkWidget *exposure_hscale;
  GtkWidget *gain_hscale;
  GtkWidget *auto_exposure_toggle;
  GtkWidget *auto_gain_toggle;

  gulong exposure_spin_changed;
  gulong gain_spin_changed;
  gulong exposure_hscale_changed;
  gulong gain_hscale_changed;
  gulong auto_gain_clicked;
  gulong auto_exposure_clicked;

  double exposure_min, exposure_max;

  guint gain_update_event;
  guint exposure_update_event;

  gulong video_window_xid;

#ifdef CAPTIO_WITH_NOTIFY
  NotifyNotification *notification;
#endif
} CaptioGUI;

typedef enum { ROI_source, ROI_save, ROI_motion, ROI_background, ROI_END }
  ROI_t;

typedef enum { Handle_NW, Handle_W, Handle_SW, Handle_S,
	       Handle_SE, Handle_E, Handle_NE, Handle_N,
	       Handle_Center, Handle_END }
  Handle_t;

typedef struct {
  gdouble x[2];
  gdouble y[2];
  Handle_t activeHandle;
  ROI_t id;
} RectangleSelection_t;

const gdouble roi_handle_size = 10;

const gchar* const ROInames[] = {
  [ROI_source] = "Acquired region",
  [ROI_save] = "Saved region",
  [ROI_motion] = "Motion detection region",
  [ROI_background] = "Still noise estimation region"
};

const gchar* const ROIbuttonnames[] = {
  [ROI_source] = "Source",
  [ROI_save] = "Save",
  [ROI_motion] = "Motion",
  [ROI_background] = "MotionRef"
};

const double ROIcolors[ROI_END][4] = {
  [ROI_source] = {1.0, 1.0, 1.0, 1.0},
  [ROI_save] = {0.0, 0.7, 0.0, 1.0},
  [ROI_motion] = {0.9, 0.0, 0.1, 1.0},
  [ROI_background] = {0.7, 0.7, 0.1, 1.0}
};

#endif /* CAPTIO_GUI_H */
