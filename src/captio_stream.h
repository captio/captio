#ifndef CAPTIO_STREAM_H
#define CAPTIO_STREAM_H

double
captio_stream_camera_get_frame_rate (CaptioCore *core);

void
captio_stream_camera_set_frame_rate (CaptioCore *core, double fps);

double
captio_stream_camera_get_exposure_time (CaptioCore *core);

void
captio_stream_camera_set_exposure_time (CaptioCore *core, double exposure);

void
captio_stream_camera_set_exposure_auto (CaptioCore *core, gboolean state);

double
captio_stream_camera_get_gain (CaptioCore *core);

void
captio_stream_camera_set_gain (CaptioCore *core, double gain);

void
captio_stream_camera_set_gain_auto (CaptioCore *core, gboolean state);

#endif /* CAPTIO_STREAM_H */
