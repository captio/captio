#ifndef CAPTIO_H
#define CAPTIO_H

typedef struct {
  gdouble x;
  gdouble y;
  gdouble w;
  gdouble h;
} Rectangle_t;

typedef struct {// CaptioCore
  gchar *outdir;
  gchar *devname;
  gdouble fps;
  guint interval;
  gdouble interval_time;
  guint burst;
  gdouble delay_time;
  Rectangle_t *acquire_roi;
  guint width;     // for comodity, equal to aquire_roi->w
  guint height;    // likewise
  Rectangle_t *save_roi;
  Rectangle_t *motion_roi;
  Rectangle_t *background_roi;
  guint hbinning;
  guint vbinning;
  gdouble exposure;
  gdouble gain;
  guint rotation;
  gboolean flip_vertical;
  gboolean flip_horizontal;

  ArvCamera *camera;
  ArvDevice *device;
  ArvStream *stream;
  ArvBuffer *last_buffer;

  GstElement *pipeline;
  GstElement *appsrc;
  GstElement *drawoverlay;
  GstElement *transform;
  GstElement *videosink;

  guint64 timestamp_offset;
  guint64 last_timestamp;

  guint state; // GdkModifier mask of last input event in drawing area
  guint keysym; // GDK key code of last key-*-event in drawing area
  gdouble ox, oy; // Pointer coordinates at last button-press-event

  gulong video_window_xid;
} CaptioCore;

#endif /* CAPTIO_H */
