/* Captio - a camera image stream recorder
 *
 * Copyright © 2013 Adrian Daerr
 * See main file captio.c for licence and disclaimer.
 *
 * This is captio_stream.c, implementing the image acquisition code.
 */

/*
 * Parameter getters and setters for the rest of captio's code
 * -----------------------------------------------------------
 */

// Note: currently camera devices are necessarily aravis devices,
// so most of the functions are merely proxies to aravis functions

double
captio_stream_camera_get_frame_rate (CaptioCore *core)
{
  core->fps = arv_camera_get_frame_rate (core->camera);
  return core->fps;
}

void
captio_stream_camera_set_frame_rate (CaptioCore *core, double fps)
{
  core->fps = fps;
  arv_camera_set_frame_rate (core->camera, fps);
}

/* Set camera to maximum framerate. */
double
captio_stream_camera_set_frame_rate_max (CaptioCore *core)
{
  double min, max;
  arv_camera_get_frame_rate_bounds (core->camera, &min, &max);
  arv_camera_set_frame_rate (core->camera, max);
  return core->fps = arv_camera_get_frame_rate (core->camera);
}

double
captio_stream_camera_get_exposure_time (CaptioCore *core)
{
  core->exposure = arv_camera_get_exposure_time (core->camera);
  return core->exposure;
}

void
captio_stream_camera_set_exposure_time (CaptioCore *core, double exposure)
{
  core->exposure = exposure;
  arv_camera_set_exposure_time (core->camera, exposure);
}

void
captio_stream_camera_set_exposure_auto (CaptioCore *core, gboolean state)
{
  arv_camera_set_exposure_auto (core->camera, state ? ARV_AUTO_CONTINUOUS : ARV_AUTO_OFF);
  if (state) 
    core->exposure = 0;
  else
    core->exposure = arv_camera_get_exposure (core->camera);
}

double
captio_stream_camera_get_gain (CaptioCore *core)
{
  core->gain = arv_camera_get_gain (core->camera);
  return core->gain;
}

void
captio_stream_camera_set_gain (CaptioCore *core, double gain)
{
  core->gain = gain;
  arv_camera_set_gain (core->camera, gain);
}

void
captio_stream_camera_set_gain_auto (CaptioCore *core, gboolean state)
{
  arv_camera_set_gain_auto (core->camera, state ? ARV_AUTO_CONTINUOUS : ARV_AUTO_OFF);
  if (state) 
    core->gain = 0;
  else
    core->gain = arv_camera_get_gain (core->camera);
}


/*
 * Aravis-related parts, including aravis gst-appsrc
 * -------------------------------------------------
 */

/* Add devices detected by aravis to the list. */
unsigned int
captio_arv_add_device_list (GtkListStore *list_store)
{
  GtkTreeIter iter;
  unsigned int i;
  unsigned int n_devices;

  arv_update_device_list ();
  n_devices = arv_get_n_devices ();
  for (i = 0; i < n_devices; i++) {
    gtk_list_store_append (list_store, &iter);
    gtk_list_store_set (list_store, &iter, 0, arv_get_device_id (i), -1);
  }

  return n_devices;
}

/* Called whenever a new buffer from arv becomes ready. Injects the
 * buffer into appsrc for further processing. */
void
captio_arv_new_buffer_cb (ArvStream *stream, ArvViewer *viewer)
{
  ArvBuffer *arv_buffer;
  GstBuffer *buffer;

  arv_buffer = arv_stream_pop_buffer (stream);
  if (arv_buffer == NULL)
    return;

  if (arv_buffer->status == ARV_BUFFER_STATUS_SUCCESS) {
    int arv_row_stride;
    buffer = gst_buffer_new ();

    arv_row_stride = arv_buffer->width * ARV_PIXEL_FORMAT_BIT_PER_PIXEL (arv_buffer->pixel_format) / 8;

    // TODO: avoid copying when buffers are compatible
    // (use GstBufferPool ?)
    int gst_row_stride;
    size_t size;
    GstMemory *memory;
    GstMapInfo info;
    /* Gstreamer requires row stride to be a multiple of 4 */
    if ((arv_row_stride & 0x3) != 0) {
      gst_row_stride = (arv_row_stride & ~(0x3)) + 4;
    } else {
      gst_row_stride = arv_row_stride;
    }
    /* allocate memory and add to the buffer */
    size = arv_buffer->height * gst_row_stride;
    memory = gst_allocator_alloc (NULL, size, NULL);

    gst_buffer_append_memory (buffer, memory);

    /* get WRITE access to the memory and copy image */
    gst_buffer_map (buffer, &info, GST_MAP_WRITE);

    if (gst_row_stride == arv_row_stride) {
      memcpy (info.data, arv_buffer->data,
	      arv_row_stride * arv_buffer->height);
    } else {
      int i;

      for (i = 0; i < arv_buffer->height; i++)
	memcpy (((char *) info.data) + i * gst_row_stride,
		((char *) arv_buffer->data) + i * arv_row_stride,
		arv_row_stride);
    }

    gst_buffer_unmap (buffer, &info);

    if (viewer->timestamp_offset == 0) {
      viewer->timestamp_offset = arv_buffer->timestamp_ns;
      viewer->last_timestamp = arv_buffer->timestamp_ns;
    }

    GST_BUFFER_DTS (buffer) = arv_buffer->timestamp_ns - viewer->timestamp_offset;
    GST_BUFFER_DURATION (buffer) = arv_buffer->timestamp_ns - viewer->last_timestamp;

    //g_print("pushing buffer: %s\n",gst_flow_get_name (
    gst_app_src_push_buffer (GST_APP_SRC (viewer->appsrc), buffer)
      // ))
      ;

  }

  if (viewer->last_buffer != NULL)
    arv_stream_push_buffer (stream, viewer->last_buffer);
  viewer->last_buffer = arv_buffer;
}

/*
 * GStreamer-related parts
 * -----------------------
 */

/* Stop gst pipeline, free resources. */
void
captio_stream_release (CaptioCore *core)
{
  g_return_if_fail (core != NULL);

  if (core->pipeline != NULL) {
    gst_element_set_state (core->pipeline, GST_STATE_NULL);
  }

  if (core->videosink != NULL && core->gui != NULL) {
    // disconnect some signals
    //g_print("Disconnected %d proxy handlers.\n", 
    g_signal_handlers_disconnect_by_func(core->gui->drawing_area, G_CALLBACK (da_gdk_event_to_gst_navigation_cb), core->core)
    //	    )
      ;
  }

  if (core->stream != NULL) {
    g_object_unref (core->stream);
    core->stream = NULL;
  }

  if (core->camera != NULL) {
    g_object_unref (core->camera);
    core->camera = NULL;
    core->device = NULL;
  }

  if (core->pipeline != NULL) {
    g_object_unref (core->pipeline);
    core->pipeline = NULL;
    core->appsrc = NULL;
  }

  if (core->last_buffer != NULL) {
    g_object_unref (core->last_buffer);
    core->last_buffer = NULL;
  }

  core->timestamp_offset = 0;
  core->last_timestamp = 0;
}
