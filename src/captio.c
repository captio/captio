/* Captio - a camera image stream recorder
 *
 * Copyright © 2013 Adrian Daerr
 *
 * based on the Aravis viewer which is
 * Copyright © 2009-2012 Emmanuel Pacaud
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Adrian Daerr <adrian@tortenboxer.de>
 */

#include <config.h>
#include <gtk/gtk.h>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <gst/video/video.h>
#include <gst/video/videooverlay.h>
#include <gdk/gdkx.h>
#include <arv.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#ifdef CAPTIO_WITH_NOTIFY
#include <libnotify/notify.h>
#endif
#include <cairo.h>

/* Save last buffer to file */
void
captio_gui_snapshot_cb (GtkButton *button, ArvViewer *viewer)
{
//  GFile *file;
//  char *path;
//  char *filename;
//  GDateTime *date;
//  char *date_string;
//
//  g_return_if_fail (ARV_IS_CAMERA (viewer->camera));
//  g_return_if_fail (ARV_IS_BUFFER (viewer->last_buffer));
//
//  path = g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_PICTURES),
//			   "Captio", NULL);
//  file = g_file_new_for_path (path);
//  g_free (path);
//  g_file_make_directory (file, NULL, NULL);
//  g_object_unref (file);
//
//  date = g_date_time_new_now_local ();
//  date_string = g_date_time_format (date, "%Y-%m-%d-%H:%M:%S");
//  filename = g_strdup_printf ("%s-%s-%d-%d-%s-%s.raw",
//			      arv_camera_get_vendor_name (viewer->camera),
//			      arv_camera_get_device_id (viewer->camera),
//			      viewer->last_buffer->width,
//			      viewer->last_buffer->height,
//			      viewer->pixel_format_string != NULL ? viewer->pixel_format_string : "Unknown",
//			      date_string);
//  path = g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_PICTURES),
//			   "Captio", filename, NULL);
//  g_file_set_contents (path, viewer->last_buffer->data, viewer->last_buffer->size, NULL);
//
//#ifdef CAPTIO_WITH_NOTIFY
//  if (viewer->notification) {
//    notify_notification_update (viewer->notification,
//				"Snapshot saved to Image folder",
//				path,
//				"gtk-save");
//    notify_notification_show (viewer->notification, NULL);
//  }
//#endif
//
//  g_free (path);
//  g_free (filename);
//  g_free (date_string);
//  g_date_time_unref (date);
}

/* Set transform method on the videoflip element according to state. */
static void
captio_gst_update_transform (ArvViewer *viewer)
{
  // typedef copy-pasted from gstreamer1.0-plugins-good source
  typedef enum {
    GST_VIDEO_FLIP_METHOD_IDENTITY,
    GST_VIDEO_FLIP_METHOD_90R,
    GST_VIDEO_FLIP_METHOD_180,
    GST_VIDEO_FLIP_METHOD_90L,
    GST_VIDEO_FLIP_METHOD_HORIZ,
    GST_VIDEO_FLIP_METHOD_VERT,
    GST_VIDEO_FLIP_METHOD_TRANS,
    GST_VIDEO_FLIP_METHOD_OTHER
  } GstVideoFlipMethod;
#define TR(x) GST_VIDEO_FLIP_METHOD_##x
  static const gint methods[4][4] = {
    {TR(IDENTITY), TR(90R), TR(180), TR(90L)},// no flip * Rot
    {TR(HORIZ), TR(VERT), TR(TRANS), TR(OTHER)},// H flip * Rot
    {TR(VERT), TR(OTHER), TR(HORIZ), TR(TRANS)},// V flip * Rot
    {TR(180), TR(90L), TR(IDENTITY), TR(90R)}// HV flip (=180) * Rot
  };
#undef TR
  int index = (viewer->flip_horizontal ? 1 : 0) + (viewer->flip_vertical ? 2 : 0);

  g_object_set (viewer->transform, "method", methods[index][viewer->rotation % 4], NULL);
}

/* GUI widget callback: rotate_cw_cb */
void
captio_gui_rotate_cw_cb (GtkButton *button, ArvViewer *viewer)
{
  viewer->rotation = (viewer->rotation + 1) % 4;

  captio_gst_update_transform (viewer);
}

/* GUI widget callback: flip_horizontal_toggle */
void
captio_gui_flip_horizontal_cb (GtkToggleButton *toggle, ArvViewer *viewer)
{
  viewer->flip_horizontal = gtk_toggle_button_get_active (toggle);

  captio_gst_update_transform (viewer);
}

/* GUI widget callback: flip_vertical_toggle */
void
captio_gui_flip_vertical_cb (GtkToggleButton *toggle, ArvViewer *viewer)
{
  viewer->flip_vertical = gtk_toggle_button_get_active (toggle);

  captio_gst_update_transform (viewer);
}

/* Draw selections on video */
void
captio_gst_cairo_draw_cb (GstElement *gstcairooverlay,
	       cairo_t          *cr, // the cairo context
	       guint64          arg2,
	       guint64          arg3,
	       gpointer         user_data)
{
  gint i;
  gdouble x_W, x_C, x_E, y_N, y_C, y_S;

  for (i = 0; i < sizeof(ROI)/sizeof(RectangleSelection_t*); i++) {
    if (ROI[i] == NULL) continue;
    x_W = ROI[i]->x[0];
    x_E = ROI[i]->x[1];
    x_C = (x_W+x_E)/2;
    y_N = ROI[i]->y[0];
    y_S = ROI[i]->y[1];
    y_C = (y_N+y_S)/2;
    cairo_rectangle(cr, x_W, y_N, x_E - x_W, y_S - y_N);
    cairo_set_source_rgba (cr, ROIcolors[i][0], ROIcolors[i][1], ROIcolors[i][2], 1.0);
    cairo_stroke (cr);
    if (i == activeSelection) {
      cairo_set_source_rgba (cr, ROIcolors[i][0], ROIcolors[i][1], ROIcolors[i][2], 0.5);
      cairo_arc(cr, x_W, y_N, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_C, y_N, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_E, y_N, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_E, y_C, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_E, y_S, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_C, y_S, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_W, y_S, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
      cairo_arc(cr, x_W, y_C, roi_handle_size, 0, 2*M_PI);
      cairo_fill(cr);
    }
  }
}

void
navigation_delete_active_selection () {
  RectangleSelection_t* selection;
  selection = ROI[activeSelection];
  if (selection != NULL) {
    ROI[activeSelection] = NULL;
    g_slice_free(RectangleSelection_t, selection);
  }
}

gboolean
navigation_event_mouse_button_press(GstEvent *event, gdouble *ox, gdouble *oy) {
  gdouble x, y;
  gint button;
  RectangleSelection_t* selection;
  Handle_t handle;

  gst_navigation_event_parse_mouse_button_event (event, &button, &x, &y);
  //g_print("navigation event: mouse-button(%d)-press at (%f, %f).\n",
  //	button, x, y);

  // Currently ROI adjustment is the only tool, so quit if none selected
  if (activeSelection >= ROI_END) return TRUE;

  if (button == 1) {
    // save current pointer position
    *ox = x;
    *oy = y;

    if (ROI[activeSelection] == NULL) {// create ROI
      selection = g_slice_new(RectangleSelection_t);
      selection->x[0] = selection->x[1] = x;
      selection->y[0] = selection->y[1] = y;
      selection->id = activeSelection;
      selection->activeHandle = Handle_SE;
      ROI[activeSelection] = selection;
    } else {
      selection = ROI[activeSelection];

      handle = selectedHandle(selection, x, y);
      if (handle < Handle_END) {
        selection->activeHandle = handle;
      } else {// outside click: start new selection at mouse pointer
        selection->x[0] = selection->x[1] = x;
        selection->y[0] = selection->y[1] = y;
        selection->activeHandle = Handle_SE;
      }
    }
  } else if (button == 3) {// destroy selection
    navigation_delete_active_selection();
  }

  return TRUE;
}

void
navigation_move_selection_handle (RectangleSelection_t* selection,
                                  Handle_t handle, 
                                  gdouble dx, double dy) {
  if (selection == NULL) return;
  if (handle == Handle_W ||
      handle == Handle_NW ||
      handle == Handle_SW ||
      handle == Handle_Center) {
    selection->x[0] += dx;
  }
  if (handle == Handle_E ||
      handle == Handle_NE ||
      handle == Handle_SE ||
      handle == Handle_Center) {
    selection->x[1] += dx;
  }
  if (handle == Handle_N ||
      handle == Handle_NW ||
      handle == Handle_NE ||
      handle == Handle_Center) {
    selection->y[0] += dy;
  }
  if (handle == Handle_S ||
      handle == Handle_SW ||
      handle == Handle_SE ||
      handle == Handle_Center) {
    selection->y[1] += dy;
  }
}

void
navigation_move_selection (RectangleSelection_t* selection, 
                           gdouble dx, double dy) {
  Handle_t handle;
  if (selection == NULL) return;
  handle = selection->activeHandle;
  navigation_move_selection_handle(selection, handle, dx, dy);
}                         

gboolean
navigation_event_mouse_move (GstEvent *event, gdouble *ox, gdouble *oy, guint state) {
  gdouble x, y, dx, dy;
  gint button;
  RectangleSelection_t* selection;

  gst_navigation_event_parse_mouse_move_event (event, &x, &y);
  //g_print("navigation event: mouse-move to (%f, %f).\n", x, y);
  button = (state & GDK_BUTTON1_MASK) ? 1 :
    ((state & GDK_BUTTON2_MASK) ? 2 :
     ((state & GDK_BUTTON3_MASK) ? 3 : 0));

  /* Currently ROI adjustment is the only tool, so quit if none selected */
  if (activeSelection >= ROI_END) return TRUE;

  selection = ROI[activeSelection];

  if (button == 1) {
    if (selection == NULL)
      return FALSE;

    // calculate increment to last pointer position
    dx = x - *ox;
    dy = y - *oy;
    // move selection
    navigation_move_selection(selection, dx, dy);
    // save current position
    *ox = x;
    *oy = y;
  }

  /* We've handled it, stop processing */
  return TRUE;
}

gboolean
navigation_event_key_press (GstEvent *event, ArvViewer *viewer) {
  guint key, state;
  RectangleSelection_t* selection;
  double xs, ys;

  selection = ROI[activeSelection];

  key = viewer->keysym;
  state = viewer->state;

  if (state & GDK_SHIFT_MASK)
    xs = ys = 1;
  else
    xs = ys = 8;

  switch (key) {
  case GDK_KEY_BackSpace: // delete selection
  case GDK_KEY_Delete:
    navigation_delete_active_selection ();
    break;
  case GDK_KEY_Return: // create selection
    g_print("yik");
    if (ROI[activeSelection] == NULL) {
      g_print("yak");
      selection = g_slice_new(RectangleSelection_t);
      selection->x[0] = viewer->width*1/4;
      selection->x[1] = viewer->width*3/4;
      selection->y[0] = viewer->height*1/4;
      selection->y[1] = viewer->height*3/4;
      selection->id = activeSelection;
      selection->activeHandle = Handle_Center;
      ROI[activeSelection] = selection;
    }
    break;
  case GDK_KEY_Tab:
    activeSelection = (activeSelection+1) % ROI_END;
    break;
  case GDK_KEY_a: // NW left
    navigation_move_selection_handle(selection, Handle_NW, -xs, 0);
    break;
  case GDK_KEY_s: // NW down
    navigation_move_selection_handle(selection, Handle_NW, 0, ys);
    break;
  case GDK_KEY_d: // NW right
    navigation_move_selection_handle(selection, Handle_NW, xs, 0);
    break;
  case GDK_KEY_w: // NW up
    navigation_move_selection_handle(selection, Handle_NW, 0, -ys);
    break;
  case GDK_KEY_j: // SE left
    navigation_move_selection_handle(selection, Handle_SE, -xs, 0);
    break;
  case GDK_KEY_k: // SE down
    navigation_move_selection_handle(selection, Handle_SE, 0, ys);
    break;
  case GDK_KEY_l: // SE right
    navigation_move_selection_handle(selection, Handle_SE, xs, 0);
    break;
  case GDK_KEY_i: // SE up
    navigation_move_selection_handle(selection, Handle_SE, 0, -ys);
    break;
  case GDK_KEY_Left: // Selection left
    navigation_move_selection_handle(selection, Handle_Center, -xs, 0);
    break;
  case GDK_KEY_Down: // Selection down
    navigation_move_selection_handle(selection, Handle_Center, 0, ys);
    break;
  case GDK_KEY_Right: // Selection right
    navigation_move_selection_handle(selection, Handle_Center, xs, 0);
    break;
  case GDK_KEY_Up: // Selection up
    navigation_move_selection_handle(selection, Handle_Center, 0, -ys);
    break;
  default:
    break;
  }
  return TRUE;
}

/* Handle navigation events in the gstreamer pipeline for the Region
   Of Interest selection. */
static GstPadProbeReturn
navigation_event_cb (GstPad *pad,
		   GstPadProbeInfo *info,
		   ArvViewer *viewer)
{
  GstEvent *event;

  event = gst_pad_probe_info_get_event(info);
  //g_print("pad probe upstream event of type %s.\n",
  //        GST_EVENT_TYPE_NAME(event));

  switch (GST_EVENT_TYPE (event)) {
  case GST_EVENT_NAVIGATION:
    {
      static gdouble ox, oy;
    
      switch (gst_navigation_event_get_type(event)) {
      case GST_NAVIGATION_EVENT_MOUSE_BUTTON_PRESS:
        navigation_event_mouse_button_press(event, &ox, &oy);
	break;
      case GST_NAVIGATION_EVENT_MOUSE_MOVE:
        navigation_event_mouse_move(event, &ox, &oy, viewer->state);
	break;
      case GST_NAVIGATION_EVENT_MOUSE_BUTTON_RELEASE:
	//gst_navigation_event_parse_mouse_button_event (event, &button,&x,&y);
	//g_print("navigation event: mouse-button(%d)-release at (%f, %f).\n",
	//	button, x, y);
	break;
      case GST_NAVIGATION_EVENT_KEY_PRESS:
        navigation_event_key_press(event, viewer);
        break;
      case GST_NAVIGATION_EVENT_KEY_RELEASE:
        //gst_navigation_event_parse_key_event (event, key);
        break;
      default:
	g_print("Unprocessed navigation event of type %d.\n",
		gst_navigation_event_get_type(event));
	break;
      }
      break;
    }
  case GST_EVENT_QOS:
    break;
  default:
    g_print("Unprocessed pad probe upstream event of type %s.\n",
	    GST_EVENT_TYPE_NAME(event));
    break;
  }

  return GST_PAD_PROBE_OK;
}

/* Main purpose: push the window-id of the video-overlay to the
 * xvimagesink element. */
static GstBusSyncReply
captio_gst_bus_sync_handler (GstBus *bus, GstMessage *message, gpointer user_data)
{
  ArvViewer *viewer = user_data;

  //g_print("message from %s: %s\n", 
  //	GST_MESSAGE_SRC_NAME (message),
  //	GST_MESSAGE_TYPE_NAME (message));
  if (GST_MESSAGE_TYPE (message) == GST_MESSAGE_ERROR) {
    GError *err = NULL;
    gchar *dbg_info = NULL;
    gst_message_parse_error (message, &err, &dbg_info);
    g_printerr ("ERROR from element %s: %s\n",
		GST_OBJECT_NAME (message->src), err->message);
    g_printerr ("Debugging info: %s\n", (dbg_info) ? dbg_info : "none");
    g_error_free (err);
    g_free (dbg_info);
  }
  if (GST_MESSAGE_TYPE (message) != GST_MESSAGE_ELEMENT)
    return GST_BUS_PASS;
  if (!gst_is_video_overlay_prepare_window_handle_message(message))
    return GST_BUS_PASS;

  if (viewer->video_window_xid != 0) {
    GstVideoOverlay *videooverlay;

    videooverlay = GST_VIDEO_OVERLAY (GST_MESSAGE_SRC (message));
    gst_video_overlay_set_window_handle (videooverlay, viewer->video_window_xid);

    if (g_strcmp0 (G_OBJECT_TYPE_NAME (videooverlay), "GstXvImageSink"))
      g_object_set (videooverlay, "draw-borders", TRUE, NULL);

    g_object_set (videooverlay, "force-aspect-ratio", TRUE, "sync", FALSE, NULL);
  } else {
    g_warning ("Should have obtained video_window_xid by now!");
  }

  gst_message_unref (message);

  return GST_BUS_DROP;
}

/* New camera selected:
 * - free old one,
 * - initialize new stream,
 * - get camera properties,
 * - update GUI from camera properties,
 * - create gstreamer pipeline
 * - install GUI handlers on video-overlay
 * - set pipeline PLAYING
 */
void
arv_viewer_select_camera_cb (GtkComboBox *combo_box, ArvViewer *viewer)
{
  GtkTreeIter iter;
  GtkTreeModel *list_store;
  GstCaps *caps;
  GstElement *videoconvert;
  GstElement *videoconvert2;
  GstElement *videoconvert3;
  GstElement *testelement;
  GstBus *bus;
  GstPad *pad;
  //ArvPixelFormat pixel_format;
  char *camera_id;
  char *string;
  unsigned int payload;
  int width;
  int height;
  unsigned int i;
  double frame_rate;
  double gain_min, gain_max;
  gboolean auto_gain, auto_exposure;
  const char *caps_string;
  gboolean is_frame_rate_available;
  gboolean is_exposure_available;
  gboolean is_exposure_auto_available;
  gboolean is_gain_available;
  gboolean is_gain_auto_available;

  g_return_if_fail (viewer != NULL);

  arv_viewer_release_camera (viewer);

  gtk_combo_box_get_active_iter (GTK_COMBO_BOX (viewer->camera_combo_box), &iter);
  list_store = gtk_combo_box_get_model (GTK_COMBO_BOX (viewer->camera_combo_box));
  gtk_tree_model_get (GTK_TREE_MODEL (list_store), &iter, 0, &camera_id, -1);
  viewer->camera = arv_camera_new (camera_id);
  g_free (camera_id);

  viewer->rotation = 0;
  viewer->stream = arv_camera_create_stream (viewer->camera, NULL, NULL);
  if (viewer->stream == NULL) {
    g_object_unref (viewer->camera);
    viewer->camera = NULL;
    return;
  }

  if (ARV_IS_GV_STREAM (viewer->stream)) {
    if (captio_option_arv_auto_socket_buffer)
      g_object_set (viewer->stream,
		    "socket-buffer", ARV_GV_STREAM_SOCKET_BUFFER_AUTO,
		    "socket-buffer-size", 0,
		    NULL);
    if (captio_option_arv_no_packet_resend)
      g_object_set (viewer->stream,
		    "packet-resend", ARV_GV_STREAM_PACKET_RESEND_NEVER,
		    NULL);
    g_object_set (viewer->stream,
		  "packet-timeout", (unsigned) captio_option_arv_packet_timeout * 1000,
		  "frame-retention", (unsigned) captio_option_arv_frame_retention * 1000,
		  NULL);
  }
  arv_stream_set_emit_signals (viewer->stream, TRUE);
  payload = arv_camera_get_payload (viewer->camera);
  for (i = 0; i < 50; i++)
    arv_stream_push_buffer (viewer->stream, arv_buffer_new (payload, NULL));

  arv_camera_get_region (viewer->camera, NULL, NULL, &width, &height);
  viewer->width = width;
  viewer->height = height;
  //pixel_format = arv_camera_get_pixel_format (viewer->camera);
  viewer->pixel_format_string = arv_camera_get_pixel_format_as_string (viewer->camera);
  arv_camera_get_exposure_time_bounds (viewer->camera, &viewer->exposure_min, &viewer->exposure_max);
  arv_camera_get_gain_bounds (viewer->camera, &gain_min, &gain_max);
  frame_rate = arv_camera_get_frame_rate (viewer->camera);
  auto_gain = arv_camera_get_gain_auto (viewer->camera) != ARV_AUTO_OFF;
  auto_exposure = arv_camera_get_gain_auto (viewer->camera) != ARV_AUTO_OFF;

  is_frame_rate_available = arv_camera_is_frame_rate_available (viewer->camera);
  is_exposure_available = arv_camera_is_exposure_time_available (viewer->camera);
  is_exposure_auto_available = arv_camera_is_exposure_auto_available (viewer->camera);
  is_gain_available = arv_camera_is_gain_available (viewer->camera);
  is_gain_auto_available = arv_camera_is_gain_auto_available (viewer->camera);

  g_signal_handler_block (viewer->gain_hscale, viewer->gain_hscale_changed);
  g_signal_handler_block (viewer->gain_spin_button, viewer->gain_spin_changed);
  g_signal_handler_block (viewer->exposure_hscale, viewer->exposure_hscale_changed);
  g_signal_handler_block (viewer->exposure_spin_button, viewer->exposure_spin_changed);

  gtk_spin_button_set_range (GTK_SPIN_BUTTON (viewer->exposure_spin_button),
			     viewer->exposure_min, viewer->exposure_max);
  gtk_spin_button_set_increments (GTK_SPIN_BUTTON (viewer->exposure_spin_button), 200.0, 1000.0);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (viewer->gain_spin_button), gain_min, gain_max);
  gtk_spin_button_set_increments (GTK_SPIN_BUTTON (viewer->gain_spin_button), 1, 10);

  gtk_range_set_range (GTK_RANGE (viewer->exposure_hscale), 0.0, 1.0);
  gtk_range_set_range (GTK_RANGE (viewer->gain_hscale), gain_min, gain_max);

  gtk_widget_set_sensitive (viewer->frame_rate_entry, is_frame_rate_available);

  string = g_strdup_printf ("%g", frame_rate);
  gtk_entry_set_text (GTK_ENTRY (viewer->frame_rate_entry), string);
  g_free (string);

  gtk_widget_set_sensitive (viewer->gain_hscale, is_gain_available);
  gtk_widget_set_sensitive (viewer->gain_spin_button, is_gain_available);

  gtk_widget_set_sensitive (viewer->exposure_hscale, is_exposure_available);
  gtk_widget_set_sensitive (viewer->exposure_spin_button, is_exposure_available);

  g_signal_handler_unblock (viewer->gain_hscale, viewer->gain_hscale_changed);
  g_signal_handler_unblock (viewer->gain_spin_button, viewer->gain_spin_changed);
  g_signal_handler_unblock (viewer->exposure_hscale, viewer->exposure_hscale_changed);
  g_signal_handler_unblock (viewer->exposure_spin_button, viewer->exposure_spin_changed);

  auto_gain = arv_camera_get_gain_auto (viewer->camera) != ARV_AUTO_OFF;
  auto_exposure = arv_camera_get_exposure_time_auto (viewer->camera) != ARV_AUTO_OFF;

  captio_gui_update_gain_ui (viewer, auto_gain);
  captio_gui_update_exposure_ui (viewer, auto_exposure);

  g_signal_handler_block (viewer->auto_gain_toggle, viewer->auto_gain_clicked);
  g_signal_handler_block (viewer->auto_exposure_toggle, viewer->auto_exposure_clicked);
	
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (viewer->auto_gain_toggle), auto_gain);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (viewer->auto_exposure_toggle), auto_exposure);

  gtk_widget_set_sensitive (viewer->auto_gain_toggle, is_gain_auto_available);
  gtk_widget_set_sensitive (viewer->auto_exposure_toggle, is_exposure_auto_available);

  g_signal_handler_unblock (viewer->auto_gain_toggle, viewer->auto_gain_clicked);
  g_signal_handler_unblock (viewer->auto_exposure_toggle, viewer->auto_exposure_clicked);

  //caps_string = arv_pixel_format_to_gst_caps_string (pixel_format);
  //if (caps_string == NULL) {
  //	g_message ("GStreamer cannot understand the camera pixel format: 0x%x!\n", (int) pixel_format);
  //	return;
  //}
  caps_string = "video/x-raw";
  //"bpp=(int)8,"
  //"depth=(int)8";

  arv_camera_start_acquisition (viewer->camera);

  viewer->pipeline = gst_pipeline_new ("pipeline");

  viewer->appsrc = gst_element_factory_make ("appsrc", NULL);
  videoconvert = gst_element_factory_make ("videoconvert", NULL);
  videoconvert2 = gst_element_factory_make ("videoconvert", NULL);
  videoconvert3 = gst_element_factory_make ("videoconvert", NULL);
  viewer->transform = gst_element_factory_make ("videoflip", NULL);
  viewer->drawoverlay = gst_element_factory_make ("cairooverlay",
						  "drawoverlay");
  testelement = gst_element_factory_make ("navigationtest", NULL);
  viewer->videosink = gst_element_factory_make ("xvimagesink", NULL);

  if (g_str_has_prefix (caps_string, "video/x-bayer")) {
    GstElement *bayer2rgb;

    bayer2rgb = gst_element_factory_make ("bayer2rgb", NULL);

    gst_bin_add_many (GST_BIN (viewer->pipeline),
		      viewer->appsrc,
		      bayer2rgb,
		      videoconvert,
		      viewer->transform,
		      viewer->drawoverlay,
		      videoconvert2,
		      viewer->videosink, NULL);
    gst_element_link_many (viewer->appsrc,
			   bayer2rgb,
			   videoconvert,
			   viewer->transform,
			   viewer->drawoverlay,
			   videoconvert2,
			   viewer->videosink, NULL);
  } else {
    gst_bin_add_many (GST_BIN (viewer->pipeline),
		      viewer->appsrc,
		      viewer->transform,
		      videoconvert,
		      viewer->drawoverlay,
		      videoconvert3,
		      testelement,
		      //videoconvert2,
		      viewer->videosink, NULL);
    gst_element_link_many (viewer->appsrc,
			   viewer->transform,
			   videoconvert,
			   viewer->drawoverlay,
			   videoconvert3,
			   testelement,
			   //videoconvert2,
			   viewer->videosink, NULL);
  }

  caps = gst_caps_from_string (caps_string);
  gst_caps_set_simple (caps,
		       "format", G_TYPE_STRING, "GRAY8",
		       "width", G_TYPE_INT, width,
		       "height", G_TYPE_INT, height,
		       "framerate", GST_TYPE_FRACTION, (unsigned int ) (1000*frame_rate), 1000,
		       "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
		       NULL);
  //g_print ("%s\n", gst_caps_to_string (caps));
  //GST_LOG ("caps are %" GST_PTR_FORMAT, caps);
  gst_app_src_set_caps (GST_APP_SRC (viewer->appsrc), caps);
  gst_caps_unref (caps);
  g_object_set (G_OBJECT (viewer->appsrc),
		"format", GST_FORMAT_TIME, NULL);
  gst_app_src_set_size (GST_APP_SRC (viewer->appsrc),
			(gint64) -1); // total stream size is not known
  gst_app_src_set_stream_type(GST_APP_SRC (viewer->appsrc), 
			      GST_APP_STREAM_TYPE_STREAM);

  g_signal_connect (viewer->drawoverlay, "draw", G_CALLBACK (captio_gst_cairo_draw_cb), NULL);

  pad = gst_element_get_static_pad (viewer->drawoverlay, "src");
  if (pad) 
    gst_pad_add_probe (pad, GST_PAD_PROBE_TYPE_EVENT_UPSTREAM,
		       (GstPadProbeCallback) navigation_event_cb,
		       viewer, NULL);
  else
    g_error("Could not add probe to src pad of drawoverlay\n");
  gst_object_unref (pad);

  bus = gst_pipeline_get_bus (GST_PIPELINE (viewer->pipeline));
  gst_bus_set_sync_handler (bus, (GstBusSyncHandler) captio_gst_bus_sync_handler, viewer, NULL);
  gst_object_unref (bus);

  // Let GDK handle events on the drawing area widget for the
  // videooverlay, and inject them into the videosink
  // via the navigation interface
  // Connect handlers to drawing area widget pointer signals
  g_signal_connect (viewer->drawing_area, "motion-notify-event",
		    G_CALLBACK (da_gdk_event_to_gst_navigation_cb), viewer->core);
  g_signal_connect (viewer->drawing_area, "button-press-event",
		    G_CALLBACK (da_gdk_event_to_gst_navigation_cb), viewer->core);
  g_signal_connect (viewer->drawing_area, "button-release-event",
		    G_CALLBACK (da_gdk_event_to_gst_navigation_cb), viewer->core);
  g_signal_connect (viewer->drawing_area, "key-press-event",
		    G_CALLBACK (da_gdk_event_to_gst_navigation_cb), viewer->core);
  g_signal_connect (viewer->drawing_area, "key-release-event",
		    G_CALLBACK (da_gdk_event_to_gst_navigation_cb), viewer->core);

  gst_element_set_state (viewer->pipeline, GST_STATE_PLAYING);

  g_signal_connect (viewer->stream, "new-buffer", 
                    G_CALLBACK (captio_arv_new_buffer_cb), viewer);
}

/* Free/unref/remove/... GUI-related stuff (exposure-/gain-events,
   notifications,... */
void
captio_gui_free (ArvViewer *viewer)
{
  g_return_if_fail (viewer != NULL);

#ifdef CAPTIO_WITH_NOTIFY
  if (viewer->notification)
    g_object_unref (viewer->notification);
#endif

  if (viewer->exposure_update_event > 0)
    g_source_remove (viewer->exposure_update_event);
  if (viewer->gain_update_event > 0)
    g_source_remove (viewer->gain_update_event);
}

/* GUI quit handler, turns off things (GUI, gst pipeline and stream)
 * before exiting the main event loop (by calling gtk_main_quit()). */
void
captio_gui_quit_cb (GtkWidget *widget, ArvViewer *viewer)
{
  captio_gui_free (viewer);
  arv_viewer_release_camera (viewer);

  gtk_main_quit ();
}

/* Called when video overlay widget is realized: note window ID for videosink */
void
captio_gst_drawing_area_realize_cb (GtkWidget *widget, CaptioCore *core)
{
  core->video_window_xid = GDK_WINDOW_XID (gtk_widget_get_window (widget));
}

/* Creates the GUI:
 * - allocates the ArvViewer structure
 * - builds the GUI and shows it
 * - connects event handlers
 */
ArvViewer *
captio_gui_new (void)
{
  GtkBuilder *builder;
  GtkCellRenderer *cell;
  ArvViewer *viewer;
  char *ui_filename;

  viewer = g_new0 (ArvViewer, 1);

  builder = gtk_builder_new ();

  ui_filename = g_build_filename (CAPTIO_DATA_DIR,
				  "captio-gtk3.ui",
				  NULL);

  if (!gtk_builder_add_from_file (builder, ui_filename, NULL))
    g_error ("The user interface file is missing ('%s')", ui_filename);

  g_free (ui_filename);

  viewer->camera_combo_box = GTK_WIDGET (gtk_builder_get_object (builder, "camera_combobox"));
  viewer->snapshot_button = GTK_WIDGET (gtk_builder_get_object (builder, "snapshot_button"));
  viewer->main_window = GTK_WIDGET (gtk_builder_get_object (builder, "main_window"));
  viewer->drawing_area = GTK_WIDGET (gtk_builder_get_object (builder, "video_drawingarea"));
  viewer->trigger_combo_box = GTK_WIDGET (gtk_builder_get_object (builder, "trigger_combobox"));
  viewer->frame_rate_entry = GTK_WIDGET (gtk_builder_get_object (builder, "frame_rate_entry"));
  viewer->exposure_spin_button = GTK_WIDGET (gtk_builder_get_object (builder, "exposure_spinbutton"));
  viewer->gain_spin_button = GTK_WIDGET (gtk_builder_get_object (builder, "gain_spinbutton"));
  viewer->exposure_hscale = GTK_WIDGET (gtk_builder_get_object (builder, "exposure_hscale"));
  viewer->gain_hscale = GTK_WIDGET (gtk_builder_get_object (builder, "gain_hscale"));
  viewer->auto_exposure_toggle = GTK_WIDGET (gtk_builder_get_object (builder, "auto_exposure_togglebutton"));
  viewer->auto_gain_toggle = GTK_WIDGET (gtk_builder_get_object (builder, "auto_gain_togglebutton"));
  viewer->rotate_cw_button = GTK_WIDGET (gtk_builder_get_object (builder, "rotate_cw_button"));
  viewer->flip_vertical_toggle = GTK_WIDGET (gtk_builder_get_object (builder, "flip_vertical_togglebutton"));
  viewer->flip_horizontal_toggle = GTK_WIDGET (gtk_builder_get_object (builder, "flip_horizontal_togglebutton"));

  g_object_unref (builder);

  cell = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (viewer->camera_combo_box), cell, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (viewer->camera_combo_box), cell, "text", 0, NULL);

  gtk_widget_set_no_show_all (viewer->trigger_combo_box, TRUE);

  g_signal_connect (viewer->drawing_area, "realize", G_CALLBACK (captio_gst_drawing_area_realize_cb), viewer->core);

  gtk_widget_set_double_buffered (viewer->drawing_area, FALSE);
  // Ask to receive events the drawing area doesn't normally
  // subscribe to.
  gtk_widget_set_events (viewer->drawing_area,
			 gtk_widget_get_events (viewer->drawing_area)
			 | GDK_BUTTON_PRESS_MASK
			 | GDK_BUTTON_RELEASE_MASK
			 | GDK_KEY_PRESS_MASK
			 | GDK_BUTTON1_MOTION_MASK);

  gtk_widget_show_all (viewer->main_window);

  g_signal_connect (viewer->main_window, "destroy", G_CALLBACK (captio_gui_quit_cb), viewer);
  g_signal_connect (viewer->snapshot_button, "clicked", G_CALLBACK (captio_gui_snapshot_cb), viewer);
  g_signal_connect (viewer->rotate_cw_button, "clicked", G_CALLBACK (captio_gui_rotate_cw_cb), viewer);
  g_signal_connect (viewer->flip_horizontal_toggle, "clicked", G_CALLBACK (captio_gui_flip_horizontal_cb), viewer);
  g_signal_connect (viewer->flip_vertical_toggle, "clicked", G_CALLBACK (captio_gui_flip_vertical_cb), viewer);
  g_signal_connect (viewer->camera_combo_box, "changed", G_CALLBACK (arv_viewer_select_camera_cb), viewer);

  g_signal_connect (viewer->frame_rate_entry, "activate", G_CALLBACK (captio_gui_frame_rate_entry_cb), viewer);
  g_signal_connect (viewer->frame_rate_entry, "focus-out-event", G_CALLBACK (captio_gui_frame_rate_entry_focus_cb), viewer);

  viewer->exposure_spin_changed = g_signal_connect (viewer->exposure_spin_button, "value-changed",
						    G_CALLBACK (captio_gui_exposure_spin_cb), viewer);
  viewer->gain_spin_changed = g_signal_connect (viewer->gain_spin_button, "value-changed",
						G_CALLBACK (captio_gui_gain_spin_cb), viewer);
  viewer->exposure_hscale_changed = g_signal_connect (viewer->exposure_hscale, "value-changed",
						      G_CALLBACK (captio_gui_exposure_scale_cb), viewer);
  viewer->gain_hscale_changed = g_signal_connect (viewer->gain_hscale, "value-changed",
						  G_CALLBACK (captio_gui_gain_scale_cb), viewer);
  viewer->auto_exposure_clicked = g_signal_connect (viewer->auto_exposure_toggle, "clicked",
						    G_CALLBACK (captio_gui_auto_exposure_cb), viewer);
  viewer->auto_gain_clicked = g_signal_connect (viewer->auto_gain_toggle, "clicked",
						G_CALLBACK (captio_gui_auto_gain_cb), viewer);

#ifdef CAPTIO_WITH_NOTIFY
  viewer->notification = notify_notification_new (NULL, NULL, NULL);
#endif

  return viewer;
}

/* Variables associated with command line options. */
static gchar *captio_option_output_dir = NULL;
static gchar *captio_option_device_name = NULL;
static gdouble captio_option_fps = 0.0;
static gint captio_option_capture_interval = 0;
static gdouble captio_option_capture_interval_time = 0.0;
static gint captio_option_burst = 0;
static gdouble captio_option_capture_delay = 0.0;
static gint captio_option_acquire_roi_left = 0;
static gint captio_option_acquire_roi_top = 0;
static gint captio_option_acquire_roi_width = 0;
static gint captio_option_acquire_roi_height = 0;
static gint captio_option_save_roi_left = 0;
static gint captio_option_save_roi_top = 0;
static gint captio_option_save_roi_width = 0;
static gint captio_option_save_roi_height = 0;
static gint captio_option_motion_roi_left = 0;
static gint captio_option_motion_roi_top = 0;
static gint captio_option_motion_roi_width = 0;
static gint captio_option_motion_roi_height = 0;
static gint captio_option_background_roi_left = 0;
static gint captio_option_background_roi_top = 0;
static gint captio_option_background_roi_width = 0;
static gint captio_option_background_roi_height = 0;
static gint captio_option_horizontal_binning = 0;
static gint captio_option_vertical_binning = 0;
static gdouble captio_option_exposure_time_us = 0.0;
static gdouble captio_option_gain = 0.0;
static gint captio_option_rotation = 0;
static gboolean captio_option_flip_vertical = FALSE;
static gboolean captio_option_flip_horizontal = FALSE;
static gboolean captio_option_no_gui = FALSE;
static gboolean captio_option_window_fit = TRUE;
static gboolean captio_option_arv_auto_socket_buffer = FALSE;
static gboolean captio_option_arv_no_packet_resend = FALSE;
static gint captio_option_arv_packet_timeout = 20;
static gint captio_option_arv_frame_retention = 100;
static gchar *captio_option_arv_debug_domains = NULL;

/* Application context command line options */
static const GOptionEntry captio_option_entries[] =
  {
    {
      "output", 'o', 0, G_OPTION_ARG_STRING,
      &captio_option_output_dir,
      "Output directory name", "<dir_name>"
    },
    {
      "device-name", 'd', 0, G_OPTION_ARG_STRING,
      &captio_option_device_name, 
      "Camera name", NULL
    },
    {
      "fps", 'f', 0, G_OPTION_ARG_DOUBLE,
      &captio_option_fps,
      "Acquisition frequency", "<frames_per_sec>"
    },
    {
      "capture-interval", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_capture_interval,
      "Number of images from the start of one capture to the next", "N"
    },
    {
      "capture-interval-time", '\0', 0, G_OPTION_ARG_DOUBLE,
      &captio_option_capture_interval_time,
      "Time separating the start of one capture from the next", "T"
    },
    {
      "burst", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_burst,
      "Capture in bursts of this many images", "N"
    },
    {
      "capture-delay", 't', 0, G_OPTION_ARG_DOUBLE,
      &captio_option_capture_delay,
      "Start first capture after delay", "t"
    },
    {
      "acquire-roi-left", 'x', 0, G_OPTION_ARG_INT,
      &captio_option_acquire_roi_left,
      "X-offset of acquired region on sensor", NULL
    },
    {
      "acquire-roi-top", 'y', 0, G_OPTION_ARG_INT,
      &captio_option_acquire_roi_top,
      "Y-offset of acquired region on sensor", NULL
    },
    {
      "acquire-roi-width", 'w', 0, G_OPTION_ARG_INT,
      &captio_option_acquire_roi_width,
      "Width of acquired region on sensor", NULL
    },
    {
      "acquire-roi-height", 'h', 0, G_OPTION_ARG_INT,
      &captio_option_acquire_roi_height,
      "Height of acquired region on sensor", NULL
    },
    {
      "save-roi-left", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_save_roi_left,
      "X-offset of saved region relative to acquired region", NULL
    },
    {
      "save-roi-top", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_save_roi_top,
      "Y-offset of saved region relative to acquired region", NULL
    },
    {
      "save-roi-width", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_save_roi_width,
      "Width of saved region", NULL
    },
    {
      "save-roi-height", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_save_roi_height,
      "Height of saved region", NULL
    },
    {
      "motion-roi-left", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_motion_roi_left,
      "X-offset of motion detection region", NULL
    },
    {
      "motion-roi-top", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_motion_roi_top,
      "Y-offset of motion detection region", NULL
    },
    {
      "motion-roi-width", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_motion_roi_width,
      "Width of motion detection region", NULL
    },
    {
      "motion-roi-height", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_motion_roi_height,
      "Height of motion detection region", NULL
    },
    {
      "background-roi-left", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_background_roi_left,
      "X-offset of background region realative to acquired region", NULL
    },
    {
      "background-roi-top", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_background_roi_top,
      "Y-offset of background region relative to acquired region", NULL
    },
    {
      "background-roi-width", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_background_roi_width,
      "Width of background (noise estimation) region", NULL
    },
    {
      "background-roi-height", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_background_roi_height,
      "Height of background (noise estimation) region", NULL
    },
    {
      "h-binning", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_horizontal_binning,	"Horizontal binning", NULL
    },
    {
      "v-binning", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_vertical_binning,		"Vertical binning", NULL
    },
    {
      "exposure", 'e', 0, G_OPTION_ARG_DOUBLE,
      &captio_option_exposure_time_us,		"Exposure time (µs)", NULL
    },
    {
      "gain", 'g', 0, G_OPTION_ARG_DOUBLE,
      &captio_option_gain,			"Gain (dB)", NULL
    },
    {
      "rotation", '\0', 0, G_OPTION_ARG_INT,
      &captio_option_rotation,
      "Rotate image N * 90deg clockwise", "N"
    },
    {
      "flip-vertical", '\0', 0, G_OPTION_ARG_NONE,
      &captio_option_flip_vertical,		"Flip image vertically", NULL
    },
    {
      "flip-horizontal", '\0', 0, G_OPTION_ARG_NONE,
      &captio_option_flip_horizontal,		"Flip image horizontally", NULL
    },
    {
      "no-gui", '\0', 0, G_OPTION_ARG_NONE,
      &captio_option_no_gui,
      "Capture directly, without displaying a Graphical User Interface", NULL
    },
    {
      "video-autoscale", '\0', 0, G_OPTION_ARG_NONE,
      &captio_option_window_fit,		"Autoscale video overlay", NULL
    },
    {
      "auto-buffer-size",			'a', 0, G_OPTION_ARG_NONE,
      &captio_option_arv_auto_socket_buffer,	"Auto socket buffer size", NULL
    },
    {
      "no-packet-resend",			'r', 0, G_OPTION_ARG_NONE,
      &captio_option_arv_no_packet_resend,	"No packet resend", NULL
    },
    {
      "packet-timeout", 			'p', 0, G_OPTION_ARG_INT,
      &captio_option_arv_packet_timeout, 	"Packet timeout (ms)", NULL
    },
    {
      "frame-retention", 			'm', 0, G_OPTION_ARG_INT,
      &captio_option_arv_frame_retention, 	"Frame retention (ms)", NULL
    },
    {
      "debug", 					'D', 0, G_OPTION_ARG_STRING,
      &captio_option_arv_debug_domains, 	"Debug domains", NULL
    },
    { NULL }
  };

/* ------- main() ------- */
int
main (int argc,char *argv[])
{
  ArvViewer *viewer;
  GOptionContext *context;
  GError *error = NULL;

  // initialize translations
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  // start the aravis thread
  arv_g_thread_init (NULL);

  // parse command line options
  context = g_option_context_new (NULL);
  g_option_context_add_main_entries (context, captio_option_entries, NULL);
  g_option_context_add_group (context, gtk_get_option_group (TRUE));
  g_option_context_add_group (context, gst_init_get_option_group ());
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_option_context_free (context);
    g_print ("Option parsing failed: %s\n", error->message);
    g_error_free (error);
    return EXIT_FAILURE;
  }

  g_option_context_free (context);

  // initialize GTK and gstreamer
  if (!captio_option_no_gui)
    gtk_init (&argc, &argv);
  gst_init (&argc, &argv);

  // set aravis debugging
  arv_debug_enable (captio_option_arv_debug_domains);

  // add fake camera for testing
  arv_enable_interface ("Fake");

#ifdef CAPTIO_WITH_NOTIFY
  notify_init ("Captio Viewer");
#endif

  // build GUI
  viewer = captio_gui_new ();

  // scan for devices and select a camera
  captio_gui_update_device_list_cb (viewer);
  arv_viewer_select_camera_cb (NULL, viewer);

  // delegate to event loop
  gtk_main ();

#ifdef CAPTIO_WITH_NOTIFY
  notify_uninit ();
#endif

  /* For debug purpose only */
  arv_shutdown ();

  return EXIT_SUCCESS;
}
