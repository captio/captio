/* Captio - a camera image stream recorder
 *
 * Copyright © 2013 Adrian Daerr
 * See main file captio.c for licence and disclaimer.
 *
 * This is captio_gui.c, implementing the graphical user interface.
 */

#include "captio.h"
#include "captio_gui.h"
#include "captio_stream.h"

static RectangleSelection_t* ROI[ROI_END];
static ROI_t activeSelection = ROI_source;

/* Distance normalized by handle size */
gdouble handleDistance(gdouble hx, gdouble hy, gdouble mx, gdouble my) {
  return ((hx-mx)*(hx-mx)+(hy-my)*(hy-my))/(roi_handle_size*roi_handle_size);
  //alternative norm: MAX(ABS(hx-mx),ABS(hy-my))/roi_handle_size;
}

/* Returns handle underneath the given point, else Handle_Center if
 * the point is inside the ROI, otherwise Handle_END. */
Handle_t selectedHandle(RectangleSelection_t* roi, gdouble mx, gdouble my) {
  Handle_t zeHandle;
  gdouble dist, mindist;
  gdouble x_W, x_C, x_E, y_N, y_C, y_S;
  x_W = roi->x[0];
  x_E = roi->x[1];
  x_C = (x_W+x_E)/2;
  y_N = roi->y[0];
  y_S = roi->y[1];
  y_C = (y_N+y_S)/2;

  // N
  zeHandle = Handle_N;
  mindist = handleDistance(x_C, y_N, mx, my);
  // W
  dist = handleDistance(x_W, y_C, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_W;
    mindist = dist;
  }
  // S
  dist = handleDistance(x_C, y_S, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_S;
    mindist = dist;
  }
  // E
  dist = handleDistance(x_E, y_C, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_E;
    mindist = dist;
  }
  // NE
  dist = handleDistance(x_E, y_N, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_NE;
    mindist = dist;
  }
  // NW
  dist = handleDistance(x_W, y_N, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_NW;
    mindist = dist;
  }
  // SW
  dist = handleDistance(x_W, y_S, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_SW;
    mindist = dist;
  }
  // SE
  dist = handleDistance(x_E, y_S, mx, my);
  if (dist < mindist) {
    zeHandle = Handle_SE;
    mindist = dist;
  }

  if (mindist > 1) {// no border handle grabbed
    if (mx >= x_W && mx <= x_E && my >= y_N && my <= y_S)
      zeHandle = Handle_Center; // whole selection grabbed
    else
      zeHandle = Handle_END;
  }

  return zeHandle;
}

/* Catch GDK events on drawing area widget (videooverlay) and forward
 * them along the gstreamer pipeline as navigation events. */
gboolean
da_gdk_event_to_gst_navigation_cb (GtkWidget *widget,
				   GdkEvent *event,
				   CaptioCore *core)
{
  GstNavigation *navigation = GST_NAVIGATION(viewer->videosink);

  switch (event->type) {
  case GDK_KEY_PRESS:
  case GDK_KEY_RELEASE:
    viewer->state = ((GdkEventKey *)event)->state;
    viewer->keysym = ((GdkEventKey *)event)->keyval;
    gst_navigation_send_key_event(navigation, (event->type == GDK_KEY_PRESS) ?
				  "key-press" : "key-release", NULL);
    break;
  case GDK_BUTTON_PRESS:
    {
      GdkEventButton *e = (GdkEventButton *)event;
      viewer->state = e->state;
      viewer->ox = e->x;
      viewer->oy = e->y;
      //g_print("gdk button press at (%f,%f), button %d, type %d, modifiers %d\n", e->x, e->y, e->button, e->type, e->state);
      gst_navigation_send_mouse_event(navigation,"mouse-button-press",
				      e->button, e->x, e->y);
      gtk_widget_grab_focus(widget);
    }
    break;
  case GDK_BUTTON_RELEASE:
    {
      GdkEventButton *e = (GdkEventButton *)event;
      viewer->state = e->state;
      //g_print("gdk button release at (%f,%f), button %d, type %d, modifiers %d\n", e->x, e->y, e->button, e->type, e->state);
      gst_navigation_send_mouse_event(navigation, "mouse-button-release",
				  e->button, e->x, e->y);
    }
    break;
  case GDK_MOTION_NOTIFY:
    {
      GdkEventMotion *e = (GdkEventMotion *)event;
      guint state;
      viewer->state = state = e->state;
      //g_print("gdk motion event to (%f, %f), state %d\n",
      //	  event->x, event->y, state);
      gst_navigation_send_mouse_event(navigation, "mouse-move",
				      0, e->x, e->y);
      if (e->is_hint) gdk_event_request_motions (e);
      break;
    }
  case GDK_2BUTTON_PRESS:  // double-click
  case GDK_3BUTTON_PRESS:  // triple-click
    break;
  default:
    g_print("Unprocessed GDK event of type %d in drawing area.\n", event->type);
    break;
  }
  return TRUE;
}

double
captio_gui_value_to_log (double value, double min, double max)
{
  if (min >= max)
    return 1.0;

  if (value < min)
    return 0.0;

  return (log10 (value) - log10 (min)) / (log10 (max) - log10 (min));
}

double
captio_gui_value_from_log (double value, double min, double max)
{
  if (min <= 0.0 || max <= 0)
    return 0.0;

  if (value > 1.0)
    return max;
  if (value < 0.0)
    return min;

  return pow (10.0, (value * (log10 (max) - log10 (min)) + log10 (min)));
}

/* Update list of input devices */
void
captio_gui_update_device_list_cb (CaptioViewer *viewer)
{
  GtkListStore *list_store;
  unsigned int n_devices = 0;

  list_store = gtk_list_store_new (1, G_TYPE_STRING);
  gtk_combo_box_set_model (GTK_COMBO_BOX (viewer->camera_combo_box), GTK_TREE_MODEL (list_store));

  /* add aravis devices */
  n_devices += captio_arv_add_device_list(list_store);

  if (n_devices > 0)
    gtk_combo_box_set_active (GTK_COMBO_BOX (viewer->camera_combo_box), 0);
  if (n_devices <= 1)
    gtk_widget_set_sensitive (viewer->camera_combo_box, FALSE);
}


/* GUI widget callback: frame_rate_entry, on "activate" */
void
captio_gui_frame_rate_entry_cb (GtkEntry *entry, CaptioViewer *viewer)
{
  char *text;
  double frame_rate;

  text = (char *) gtk_entry_get_text (entry);

  captio_stream_camera_set_frame_rate (viewer->core, g_strtod (text, NULL));

  frame_rate = captio_stream_camera_get_frame_rate (viewer->core);
  text = g_strdup_printf ("%g", frame_rate);
  gtk_entry_set_text (entry, text);
  g_free (text);
}

/* GUI widget callback: frame_rate_entry, on "focus-out-event" */
static gboolean
captio_gui_frame_rate_entry_focus_cb (GtkEntry *entry, GdkEventFocus *event,
				      CaptioViewer *viewer)
{
  captio_gui_frame_rate_entry_cb (entry, viewer);

  return FALSE;
}

/* GUI widget callback: exposure_spinbutton */
void
captio_gui_exposure_spin_cb (GtkSpinButton *spin_button, CaptioViewer *viewer)
{
  double exposure = gtk_spin_button_get_value (spin_button);
  double log_exposure = captio_gui_value_to_log (exposure, viewer->exposure_min, viewer->exposure_max);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (viewer->auto_exposure_toggle), FALSE);

  captio_stream_camera_set_exposure_time (viewer->core, exposure);

  g_signal_handler_block (viewer->exposure_hscale, viewer->exposure_hscale_changed);
  gtk_range_set_value (GTK_RANGE (viewer->exposure_hscale), log_exposure);
  g_signal_handler_unblock (viewer->exposure_hscale, viewer->exposure_hscale_changed);
}

/* GUI widget callback: gain_spinbutton */
void
captio_gui_gain_spin_cb (GtkSpinButton *spin_button, CaptioViewer *viewer)
{
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (viewer->auto_gain_toggle), FALSE);

  captio_stream_camera_set_gain (viewer->core, gtk_spin_button_get_value (spin_button));

  g_signal_handler_block (viewer->gain_hscale, viewer->gain_hscale_changed);
  gtk_range_set_value (GTK_RANGE (viewer->gain_hscale), gtk_spin_button_get_value (spin_button));
  g_signal_handler_unblock (viewer->gain_hscale, viewer->gain_hscale_changed);
}

/* GUI widget callback: exposure_hscale */
void
captio_gui_exposure_scale_cb (GtkRange *range, CaptioViewer *viewer)
{
  double log_exposure = gtk_range_get_value (range);
  double exposure = captio_gui_value_from_log (log_exposure, viewer->exposure_min, viewer->exposure_max);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (viewer->auto_exposure_toggle), FALSE);

  captio_stream_camera_set_exposure_time (viewer->core, exposure);

  g_signal_handler_block (viewer->exposure_spin_button, viewer->exposure_spin_changed);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (viewer->exposure_spin_button), exposure);
  g_signal_handler_unblock (viewer->exposure_spin_button, viewer->exposure_spin_changed);
}

/* GUI widget callback: gain_hscale */
void
captio_gui_gain_scale_cb (GtkRange *range, CaptioViewer *viewer)
{
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (viewer->auto_gain_toggle), FALSE);

  captio_stream_camera_set_gain (viewer->core, gtk_range_get_value (range));

  g_signal_handler_block (viewer->gain_spin_button, viewer->gain_spin_changed);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (viewer->gain_spin_button), gtk_range_get_value (range));
  g_signal_handler_unblock (viewer->gain_spin_button, viewer->gain_spin_changed);
}

/* Update GUI exposure value according to camera settings. Is called
 * periodically from main loop when camera operates in automatic exposure
 * mode. */
gboolean
captio_gui_update_exposure_cb (void *data)
{
  CaptioViewer *viewer = data;
  double exposure;
  double log_exposure;

  exposure = captio_stream_camera_get_exposure_time (viewer->core);
  log_exposure = captio_gui_value_to_log (exposure, viewer->exposure_min, viewer->exposure_max);

  g_signal_handler_block (viewer->exposure_hscale, viewer->exposure_hscale_changed);
  g_signal_handler_block (viewer->exposure_spin_button, viewer->exposure_spin_changed);
  gtk_range_set_value (GTK_RANGE (viewer->exposure_hscale), log_exposure);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (viewer->exposure_spin_button), exposure);
  g_signal_handler_unblock (viewer->exposure_spin_button, viewer->exposure_spin_changed);
  g_signal_handler_unblock (viewer->exposure_hscale, viewer->exposure_hscale_changed);

  return TRUE;
}

/* Called from captio_gui_auto_exposure_cb(), and sets
 * captio_gui_update_exposure_cb() to be called every second so that the
 * GUI reflects the instantaneous camera exposure. */
void
captio_gui_update_exposure_ui (CaptioViewer *viewer, gboolean is_auto)
{
  captio_gui_update_exposure_cb (viewer);

  if (viewer->exposure_update_event > 0) {
    g_source_remove (viewer->exposure_update_event);
    viewer->exposure_update_event = 0;
  }

  if (is_auto)
    viewer->exposure_update_event = g_timeout_add_seconds (1, captio_gui_update_exposure_cb, viewer);
}

/* GUI widget callback: auto_exposure_toggle */
void
captio_gui_auto_exposure_cb (GtkToggleButton *toggle, CaptioViewer *viewer)
{
  gboolean is_auto;

  is_auto = gtk_toggle_button_get_active (toggle);

  captio_stream_camera_set_exposure_time_auto (viewer->core, is_auto);
  captio_gui_update_exposure_ui (viewer, is_auto);
}

/* Update GUI gain value according to camera settings. Is called
 * periodically from main loop when camera operates in automatic gain
 * mode. */
gboolean
captio_gui_update_gain_cb (void *data)
{
  CaptioViewer *viewer = data;
  double gain;

  gain = captio_stream_camera_get_gain (viewer->core);

  g_signal_handler_block (viewer->gain_hscale, viewer->gain_hscale_changed);
  g_signal_handler_block (viewer->gain_spin_button, viewer->gain_spin_changed);
  gtk_range_set_value (GTK_RANGE (viewer->gain_hscale), gain);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (viewer->gain_spin_button), gain);
  g_signal_handler_unblock (viewer->gain_spin_button, viewer->gain_spin_changed);
  g_signal_handler_unblock (viewer->gain_hscale, viewer->gain_hscale_changed);

  return TRUE;
}

/* Called from captio_gui_auto_gain_cb(), and sets
 * captio_gui_update_gain_cb() to be called every second so that the
 * GUI reflects the instantaneous camera gain. */
void
captio_gui_update_gain_ui (CaptioViewer *viewer, gboolean is_auto)
{
  captio_gui_update_gain_cb (viewer);

  if (viewer->gain_update_event > 0) {
    g_source_remove (viewer->gain_update_event);
    viewer->gain_update_event = 0;
  }

  if (is_auto)
    viewer->gain_update_event = g_timeout_add_seconds (1, captio_gui_update_gain_cb, viewer);

}

/* GUI widget callback: auto_gain_toggle */
void
captio_gui_auto_gain_cb (GtkToggleButton *toggle, CaptioViewer *viewer)
{
  gboolean is_auto;

  is_auto = gtk_toggle_button_get_active (toggle);

  captio_stream_camera_set_gain_auto (viewer->core, is_auto);
  captio_gui_update_gain_ui (viewer, is_auto);
}
